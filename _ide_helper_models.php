<?php
/**
 * A helper file for your Eloquent Models
 * Copy the phpDocs from this file to the correct Model,
 * And remove them from this file, to prevent double declarations.
 *
 * @author Barry vd. Heuvel <barryvdh@gmail.com>
 */


namespace App{
/**
 * App\Cat
 *
 * @property int $id
 * @property string $name
 * @property string $weight
 * @property string|null $temperament
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Hobby[] $hobbies
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Cat whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Cat whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Cat whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Cat whereTemperament($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Cat whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Cat whereWeight($value)
 */
	class Cat extends \Eloquent {}
}

namespace App{
/**
 * App\Hobby
 *
 * @property int $id
 * @property string $name
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Cat[] $cats
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Hobby whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Hobby whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Hobby whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Hobby whereUpdatedAt($value)
 */
	class Hobby extends \Eloquent {}
}

namespace App{
/**
 * App\User
 *
 */
	class User extends \Eloquent {}
}

