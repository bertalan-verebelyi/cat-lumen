<?php

namespace App\Http\Controllers\Api\V1;

use App\Cat;
use App\Http\Controllers\Controller;
use Request;

class CatsController extends Controller
{
    public function index()
    {
        $cats = Cat::with('hobbies')->get()->map(function (Cat $cat) {
            return $cat->attributesToArray(['id', 'name', 'weight', 'temperament', 'hobbies' => ['id', 'name']]);
        });

        return $cats;
    }

    public function show($id)
    {
        $cat = Cat::find($id)->load('hobbies');

        return $cat;
    }
}
