<?php

namespace App\Http\Controllers\Api\V1;

use App\Hobby;
use App\Http\Controllers\Controller;

class HobbiesController extends Controller
{
    public function index()
    {
        return Hobby::with('cats')->get();
    }

    public function show($id)
    {
        $hobby = Hobby::find($id)->load('cats');

        return $hobby;
    }
}
