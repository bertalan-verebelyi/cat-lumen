<?php

namespace App;

use App\Traits\Transformable;
use Illuminate\Database\Eloquent\Model;

class Hobby extends Model
{
    use Transformable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name'];

    /**
     * The cats that belong to the hobby.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function cats()
    {
        return $this->belongsToMany(Cat::class);
    }
}