<?php

namespace App\Traits;

use Illuminate\Database\Eloquent\Collection;

trait Transformable
{
    private $transformed;

    public function attributesToArray(array $attributes = [])
    {
        if (!count($attributes)) {
            return $this->toArray();
        }
        $this->transformed = [];

        collect($attributes)->each(function ($attribute, $index) {
            if (!is_array($attribute)) {
                $this->add($attribute, $this->$attribute);
                return;
            }
            if (get_class($this->$index) !== Collection::class) {
                $this->add($index, $this->$index->attributesToArray($attribute));
                return;
            }
            $this->add($index, $this->relationAttributesToArray($index, $attribute));
        });

        return $this->transformed;
    }

    private function relationAttributesToArray($relation, $attribute)
    {
        return $this->$relation->map(function ($relation) use ($attribute) {
            return $relation->attributesToArray($attribute);
        });
    }

    private function add($key, $value)
    {
        $this->transformed[$key] = $value;
    }
}