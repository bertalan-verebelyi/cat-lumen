<?php

namespace App;

use App\Traits\Transformable;
use Illuminate\Database\Eloquent\Model;

class Cat extends Model
{
    use Transformable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'breed', 'weight', 'temperament'
    ];

    /**
     * The hobbies that belong to the cat.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function hobbies()
    {
        return $this->belongsToMany(Hobby::class);
    }
}