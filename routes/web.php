<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$app->get('/', function () use ($app) {
    return $app->version();
});

$app->group(['namespace' => 'Api\V1', 'prefix' => 'api/v1'], function () use ($app) {
    $app->get('/cats', 'CatsController@index');
    $app->get('/cats/{id}', 'CatsController@show');

    $app->get('/hobbies', 'HobbiesController@index');
    $app->get('/hobbies/{id}', 'HobbiesController@show');
});
