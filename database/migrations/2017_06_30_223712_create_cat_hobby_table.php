<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCatHobbyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cat_hobby', function (Blueprint $table) {
            $table->integer('cat_id')->unsigned();
            $table->integer('hobby_id')->unsigned();

            $table->foreign('cat_id')
                ->references('id')
                ->on('cats')
                ->onDelete('cascade');

            $table->foreign('hobby_id')
                ->references('id')
                ->on('hobbies')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cat_hobby');
    }
}
