<?php

use App\Cat;
use App\Hobby;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class CatAndHobbySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::unprepared('SET FOREIGN_KEY_CHECKS = 0');
        DB::table('cats')->truncate();
        DB::table('hobbies')->truncate();
        DB::table('cat_hobby')->truncate();
        DB::unprepared('SET FOREIGN_KEY_CHECKS = 1');
        $faker = Faker\Factory::create();

        $createdAt = Carbon::now();
        $weights = [
            'slim',
            'fat',
            'normal',
            'athletic'
        ];
        $catLimit = 50;
        for ($i = 0; $i < $catLimit; $i++) {
            DB::table('cats')->insert([
                'name' => $faker->name,
                'weight' => $faker->randomElement($weights),
                'temperament' => $faker->randomElement([
                    'sanguine',
                    'choleric',
                    'melancholic',
                    'phlegmatic'
                ]),
                'created_at' => $createdAt,
                'updated_at' => $createdAt
            ]);
        }

        $hobbies = [
            'sleep',
            'play',
            'run',
            'eat'
        ];
        for ($i = 0; $i < count($hobbies); $i++) {
            DB::table('hobbies')->insert([
                'name' => $hobbies[$i]
            ]);
        }

        $hobbies = Hobby::all();
        Cat::all()->each(function (Cat $cat) use ($faker, $hobbies) {
            $hobbies = $hobbies->shuffle();
            $limit = $faker->numberBetween(2, 4);
            for ($i = 0; $i < $limit; $i++) {
                $cat->hobbies()->save($hobbies[$i]);
            }
        });
    }
}
